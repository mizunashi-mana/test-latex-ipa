#!/usr/bin/env bash

set -euo pipefail

export TEXFONTMAPS="./fonts//:${TEXFONTMAPS:-}"
export TTFONTS="./fonts//:$(kpsewhich -show-path='truetype fonts')"
export OPENTYPEFONTS="./fonts//:$(kpsewhich -show-path='opentype fonts')"
export T1FONTS="./fonts//:$(kpsewhich -show-path='type1 fonts')"

platex article.tex
dvipdfmx -f font.map article.dvi
